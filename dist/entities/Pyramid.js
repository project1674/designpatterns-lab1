import { Point } from './Point.js';
export class Pyramid {
    constructor(id, name, base, height) {
        this.id = id;
        this.name = name;
        this.base = base;
        this.height = height;
        this.observers = [];
        this.id = id;
    }
    isPyramid() {
        return this.base.every(point => point instanceof Point);
    }
    getBaseArea() {
        if (this.base.length === 3) {
            // we have a triangle base
            let a = Math.sqrt(Math.pow((this.base[0].x - this.base[1].x), 2) + Math.pow((this.base[0].y - this.base[1].y), 2));
            let b = Math.sqrt(Math.pow((this.base[1].x - this.base[2].x), 2) + Math.pow((this.base[1].y - this.base[2].y), 2));
            let c = Math.sqrt(Math.pow((this.base[2].x - this.base[0].x), 2) + Math.pow((this.base[2].y - this.base[0].y), 2));
            let s = (a + b + c) / 2;
            return Math.sqrt(s * (s - a) * (s - b) * (s - c));
        }
        else if (this.base.length === 4) {
            // we have a rectangle base
            let a = Math.sqrt(Math.pow((this.base[0].x - this.base[1].x), 2) + Math.pow((this.base[0].y - this.base[1].y), 2));
            let b = Math.sqrt(Math.pow((this.base[1].x - this.base[2].x), 2) + Math.pow((this.base[1].y - this.base[2].y), 2));
            return a * b;
        }
        else {
            throw new Error("Invalid base shape for pyramid");
        }
    }
    getSlantArea() {
        return this.base.map((point, index) => {
            let nextPoint = this.base[(index + 1) % this.base.length];
            let base = Math.sqrt(Math.pow((point.x - nextPoint.x), 2) + Math.pow((point.y - nextPoint.y), 2));
            let slantHeight;
            if (this.base.length === 4) {
                let halfDiagonal = base / 2;
                slantHeight = Math.sqrt(Math.pow(this.height, 2) + Math.pow(halfDiagonal, 2));
            }
            else if (this.base.length === 3) {
                slantHeight = Math.sqrt(Math.pow(this.height, 2) + Math.pow(base / 2, 2));
            }
            else {
                throw new Error("Invalid base shape for pyramid");
            }
            return 0.5 * base * slantHeight;
        }).reduce((a, b) => a + b);
    }
    getSurfaceArea() {
        return this.getSlantArea() + this.getBaseArea();
    }
    getPerimeter() {
        return this.base.map((point, index) => {
            let nextPoint = this.base[(index + 1) % this.base.length];
            return Math.sqrt(Math.pow((point.x - nextPoint.x), 2) + Math.pow((point.y - nextPoint.y), 2));
        }).reduce((a, b) => a + b);
    }
    getVolume() {
        return (1 / 3) * this.getBaseArea() * this.height;
    }
    baseOnCoordinatePlane() {
        const xValues = this.base.map(point => point.x);
        const yValues = this.base.map(point => point.y);
        const zValues = this.base.map(point => point.z);
        if (xValues.every(xVal => xVal === xValues[0])) {
            // All x-coordinates are the same, so the base is on the YZ plane
            return "YZ";
        }
        if (yValues.every(yVal => yVal === yValues[0])) {
            // All y-coordinates are the same, so the base is on the XZ plane
            return "XZ";
        }
        if (zValues.every(zVal => zVal === zValues[0])) {
            // All z-coordinates are the same, so the base is on the XY plane
            return "XY";
        }
        // The base is not on any of the coordinate planes
        return "no plane";
    }
    registerObserver(observer) {
        this.observers.push(observer);
    }
    unregisterObserver(observer) {
        const removeIndex = this.observers.findIndex(obs => obs === observer);
        if (removeIndex !== -1) {
            this.observers.splice(removeIndex, 1);
        }
    }
    notifyObservers() {
        this.observers.forEach(observer => observer.update(this));
    }
}
