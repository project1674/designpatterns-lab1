export class Point {
    constructor(x, y, z = 0) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    distanceTo(otherPoint) {
        const dx = otherPoint.x - this.x;
        const dy = otherPoint.y - this.y;
        const dz = otherPoint.z - this.z;
        return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }
}
