export class Rectangle {
    constructor(id, name, topLeft, topRight, bottomRight, bottomLeft) {
        this.id = id;
        this.name = name;
        this.topLeft = topLeft;
        this.topRight = topRight;
        this.bottomRight = bottomRight;
        this.bottomLeft = bottomLeft;
        this.observers = [];
        this.id = id;
    }
    getArea() {
        const width = this.topRight.distanceTo(this.topLeft);
        const height = this.topRight.distanceTo(this.bottomRight);
        return width * height;
    }
    getPerimeter() {
        const width = this.topRight.distanceTo(this.topLeft);
        const height = this.topRight.distanceTo(this.bottomRight);
        return 2 * (width + height);
    }
    isSquare() {
        const sides = [
            this.topLeft.distanceTo(this.topRight),
            this.topRight.distanceTo(this.bottomRight),
            this.bottomRight.distanceTo(this.bottomLeft),
            this.bottomLeft.distanceTo(this.topLeft),
        ];
        return sides.every(side => side === sides[0]);
    }
    isRhombus() {
        return this.isSquare();
    }
    registerObserver(observer) {
        this.observers.push(observer);
    }
    unregisterObserver(observer) {
        const removeIndex = this.observers.findIndex(obs => obs === observer);
        if (removeIndex !== -1) {
            this.observers.splice(removeIndex, 1);
        }
    }
    notifyObservers() {
        this.observers.forEach(observer => observer.update(this));
    }
}
