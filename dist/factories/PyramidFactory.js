import { Pyramid } from '../entities/Pyramid.js';
export class PyramidFactory {
    create(id, name, base, height) {
        return new Pyramid(id, name, base, height);
    }
}
