import { Point } from '../entities/Point.js';
export class PointFactory {
    static create2DPoint(x, y) {
        return new Point(x, y);
    }
    static create3DPoint(x, y, z) {
        return new Point(x, y, z);
    }
}
