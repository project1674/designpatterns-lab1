import { Rectangle } from '../entities/Rectangle.js';
import { Pyramid } from '../entities/Pyramid.js';
export class Warehouse {
    constructor() {
        this.rectangles = new Map();
        this.pyramids = new Map();
    }
    static getInstance() {
        if (!Warehouse.instance) {
            Warehouse.instance = new Warehouse();
        }
        return Warehouse.instance;
    }
    registerRectangle(rectangle) {
        rectangle.registerObserver(this);
        this.updateRectangle(rectangle);
    }
    registerPyramid(pyramid) {
        pyramid.registerObserver(this);
        this.updatePyramid(pyramid);
    }
    updateRectangle(rectangle) {
        this.rectangles.set(rectangle.id, {
            getArea: () => rectangle.getArea(),
            isSquare: () => rectangle.isSquare(),
            isRhombus: () => rectangle.isRhombus(),
        });
    }
    updatePyramid(pyramid) {
        this.pyramids.set(pyramid.id, {
            getVolume: () => pyramid.getVolume(),
            getBaseArea: () => pyramid.getBaseArea(),
            getSlantArea: () => pyramid.getSlantArea(),
            getSurfaceArea: () => pyramid.getSurfaceArea(),
            getPerimeter: () => pyramid.getPerimeter(),
            baseOnCoordinatePlane: () => pyramid.baseOnCoordinatePlane()
        });
    }
    update(observable) {
        if (observable instanceof Rectangle) {
            this.updateRectangle(observable);
        }
        else if (observable instanceof Pyramid) {
            this.updatePyramid(observable);
        }
    }
    // Дополнительные методы для доступа к данным
    getRectangleData(id) {
        return this.rectangles.get(id);
    }
    getPyramidData(id) {
        return this.pyramids.get(id);
    }
    getAllRectangles() {
        return this.rectangles;
    }
    getAllPyramids() {
        return this.pyramids;
    }
}
export default Warehouse;
