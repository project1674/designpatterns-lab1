import { Point } from '../entities/Point.js';
import { Rectangle } from '../entities/Rectangle.js';
import { Pyramid } from '../entities/Pyramid.js';
import logger from '../logger/Logger.js';
class DataValidator {
    isPointInvalid(x, y, z) {
        const numericalX = Number.parseFloat(x);
        const numericalY = Number.parseFloat(y);
        const numericalZ = Number.parseFloat(z);
        const isInvalid = Number.isNaN(numericalX) || Number.isNaN(numericalY) || Number.isNaN(numericalZ);
        if (isInvalid) {
            logger.error(`Invalid point - X: ${x}, Y: ${y}, Z: ${z}`);
        }
        return isInvalid;
    }
    validateRectangle(id, points) {
        if (points.length !== 4) {
            logger.error('Invalid rectangle - expected 4 points');
            return false;
        }
        try {
            const rectangle = new Rectangle(id, 'rectangle', points[0], points[1], points[2], points[3]);
            return rectangle.getArea() > 0 && rectangle.getPerimeter() > 0;
        }
        catch (error) {
            logger.error(error.message);
            return false;
        }
    }
    validatePyramid(id, base, height) {
        if (base.length < 3 || base.length > 4) {
            logger.error('Invalid base for pyramid - expected 3 or 4 points');
            return false;
        }
        if (height <= 0) {
            logger.error('Invalid pyramid - height should be greater than 0');
            return false;
        }
        try {
            const pyramid = new Pyramid(id, "pyramid", base, height);
            return pyramid.getVolume() > 0 && pyramid.getSurfaceArea() > 0 && pyramid.isPyramid();
        }
        catch (error) {
            logger.error(error.message);
            return false;
        }
    }
    validateLine(line) {
        const parameters = line.split(" ");
        const shapeType = parameters[0];
        let points = [];
        switch (shapeType) {
            case 'R':
                // 1 (R) + 1 (id) + 4 * 3 (points) = 14 parameters
                if (parameters.length !== 14) {
                    logger.error(`Invalid number of parameters for Rectangle. Expected 14, got ${parameters.length}`);
                    return false;
                }
                return this.isValidPoints(parameters.slice(2), points) && this.validateRectangle(parameters[1], points);
            case 'P':
                // 1 (P) + 1 (id) + 3 * 3 (points) + 1 (height) = 11 parameters
                // 1 (P) + 1 (id) + 4 * 3 (points) + 1 (height) = 14 parameters
                if (parameters.length !== 12 && parameters.length !== 15) {
                    logger.error(`Invalid number of parameters for Pyramid. Expected 11 or 14, got ${parameters.length}`);
                    return false;
                }
                return this.isValidPoints(parameters.slice(2, parameters.length - 1), points) && this.validatePyramid(parameters[1], points, parseFloat(parameters[parameters.length - 1]));
            default:
                logger.error(`Unknown shape type: ${shapeType}`);
                return false;
        }
    }
    isValidPoints(parameters, points) {
        for (let i = 0; i < parameters.length; i += 3) {
            if (this.isPointInvalid(parameters[i], parameters[i + 1], parameters[i + 2])) {
                return false;
            }
            points.push(new Point(parseFloat(parameters[i]), parseFloat(parameters[i + 1]), parseFloat(parameters[i + 2])));
        }
        return true;
    }
}
export { DataValidator };
