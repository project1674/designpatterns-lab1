export class ComparatorByName {
    compare(a, b) {
        return a.name.localeCompare(b.name);
    }
}
