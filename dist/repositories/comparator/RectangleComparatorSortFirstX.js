export class ComparatorByFirstPointXForRectangle {
    compare(a, b) {
        return a.topLeft.x - b.topLeft.x;
    }
}
