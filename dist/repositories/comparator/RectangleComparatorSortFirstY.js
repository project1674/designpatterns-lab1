export class ComparatorByFirstPointYForRectangle {
    compare(a, b) {
        return a.topLeft.y - b.topLeft.y;
    }
}
