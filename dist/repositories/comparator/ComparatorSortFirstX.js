class SortRectangleByFirstPointX {
    compare(a, b) {
        return a.topLeft.x - b.topLeft.x;
    }
}
class SortPyramidByFirstPointX {
    compare(a, b) {
        return a.base[0].x - b.base[0].x;
    }
}
export {};
