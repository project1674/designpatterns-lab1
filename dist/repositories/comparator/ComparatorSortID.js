export class ComparatorById {
    compare(a, b) {
        const idA = parseInt(a.id, 10);
        const idB = parseInt(b.id, 10);
        if (idA < idB)
            return -1;
        if (idA > idB)
            return 1;
        return 0;
    }
}
