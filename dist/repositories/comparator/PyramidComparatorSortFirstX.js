export class ComparatorByFirstPointXForPyramid {
    compare(a, b) {
        return a.base[0].x - b.base[0].x;
    }
}
