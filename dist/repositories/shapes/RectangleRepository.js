var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { BaseRepository } from '../base/BaseRepository.js';
export class RectangleRepository extends BaseRepository {
    constructor() {
        super(...arguments);
        this._store = new Map();
    }
    create(item) {
        return __awaiter(this, void 0, void 0, function* () {
            this._store.set(item.id, item);
            return this._store.has(item.id);
        });
    }
    update(id, item) {
        return __awaiter(this, void 0, void 0, function* () {
            this._store.set(id, item);
            return this._store.has(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._store.delete(id);
        });
    }
    find(item) {
        return __awaiter(this, void 0, void 0, function* () {
            return Array.from(this._store.values()).filter(rectangle => (!item.id || rectangle.id === item.id) &&
                (!item.name || rectangle.name === item.name));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._store.get(id);
        });
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return Array.from(this._store.values());
        });
    }
}
