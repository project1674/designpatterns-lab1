var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { readFile } from './file-reader/FileReader.js';
import { DataValidator } from './validators/Validators.js';
import logger from './logger/Logger.js';
import * as path from 'path';
import { Warehouse } from './warehouse/Warehouse.js';
import { Rectangle } from './entities/Rectangle.js';
import { Pyramid } from './entities/Pyramid.js';
import { Point } from './entities/Point.js';
;
import { RectangleRepository } from './repositories/shapes/RectangleRepository.js';
import { PyramidRepository } from './repositories/shapes/PyramidRepository.js';
import { ComparatorById } from './repositories/comparator/ComparatorSortID.js';
import { ComparatorByName } from './repositories/comparator/ComparatorSortName.js';
import { ComparatorByFirstPointYForRectangle } from './repositories/comparator/RectangleComparatorSortFirstY.js';
import { ComparatorByFirstPointXForRectangle } from './repositories/comparator/RectangleComparatorSortFirstX.js';
import { ComparatorByFirstPointXForPyramid } from './repositories/comparator/PyramidComparatorSortFirstX.js';
import { ComparatorByFirstPointYForPyramid } from './repositories/comparator/PyramidComparatorSortFirstY.js';
// Функция для форматирования базовых точек пирамид
function formatPyramidBase(points) {
    return points.map(point => `(${point.x}, ${point.y}, ${point.z})`).join(', ');
}
const dataValidator = new DataValidator();
const pyramidRepository = new PyramidRepository();
const rectangleRepository = new RectangleRepository();
const warehouse = Warehouse.getInstance();
const filePath = path.resolve("dist", '../shapes-data/shapes.txt');
const lines = readFile(filePath);
lines.forEach((line, index) => {
    const isValid = dataValidator.validateLine(line);
    if (isValid) {
        logger.info(`Line ${index + 1} is valid`);
    }
    else {
        logger.error(`Line ${index + 1} is invalid`);
    }
});
// Обработка валидных строк и регистрация фигур
lines.forEach((line, index) => {
    const isValid = dataValidator.validateLine(line);
    if (isValid) {
        const parameters = line.split(" ");
        const shapeType = parameters[0];
        logger.info(`Processing Line ${index + 1}: ${line}`);
        logger.info(`Shape Type: ${shapeType}`);
        logger.info(`Parameters length: ${parameters.length}`);
        logger.info(`Parameters: ${parameters.join(',')}`);
        if (shapeType === 'R') {
            logger.info(`Parameters for Rectangle: ${parameters}`);
            if (parameters.length === 14) {
                const points = [
                    new Point(parseFloat(parameters[2]), parseFloat(parameters[3]), parseFloat(parameters[4])),
                    new Point(parseFloat(parameters[5]), parseFloat(parameters[6]), parseFloat(parameters[7])),
                    new Point(parseFloat(parameters[8]), parseFloat(parameters[9]), parseFloat(parameters[10])),
                    new Point(parseFloat(parameters[11]), parseFloat(parameters[12]), parseFloat(parameters[13]))
                ];
                const nameArray = ['ARectangle', 'BRectangle', 'CRectangle', 'DRectangle', 'ERectangle', 'FRectangle', 'GRectangle'];
                const name = nameArray[index % nameArray.length];
                const rectangle = new Rectangle(parameters[1], name, points[0], points[1], points[2], points[3]);
                rectangleRepository.create(rectangle);
                warehouse.registerRectangle(rectangle);
            }
            else {
                logger.error(`Line ${index + 1} does not have correct number of parameters for a Rectangle`);
            }
        }
        else if (shapeType === 'P') {
            logger.info(`Parameters for Pyramid: ${parameters}`);
            if (parameters.length === 12 || parameters.length === 15) {
                const points = [
                    new Point(parseFloat(parameters[2]), parseFloat(parameters[3]), parseFloat(parameters[4])),
                    new Point(parseFloat(parameters[5]), parseFloat(parameters[6]), parseFloat(parameters[7])),
                    new Point(parseFloat(parameters[8]), parseFloat(parameters[9]), parseFloat(parameters[10]))
                ];
                if (parameters.length === 15) {
                    points.push(new Point(parseFloat(parameters[11]), parseFloat(parameters[12]), parseFloat(parameters[13])));
                }
                const nameArray = ['APyramid', 'BPyramid', 'CPyramid', 'DPyramid', 'EPyramid'];
                const name = nameArray[index % nameArray.length];
                const pyramid = new Pyramid(parameters[1], name, points, parseFloat(parameters[parameters.length - 1]));
                pyramidRepository.create(pyramid);
                warehouse.registerPyramid(pyramid);
            }
            else {
                logger.error(`Line ${index + 1} does not have correct number of parameters for a Pyramid`);
            }
        }
        else {
            logger.error(`Line ${index + 1} has unknown shape type`);
        }
    }
});
// Примеры поиска и сортировки
(() => __awaiter(void 0, void 0, void 0, function* () {
    // Поиск по ID
    const rectangle = yield rectangleRepository.findOne("1");
    const pyramid = yield pyramidRepository.findOne("8");
    console.log('Rectangle:', rectangle);
    if (pyramid) {
        console.log('Pyramid:', Object.assign(Object.assign({}, pyramid), { base: formatPyramidBase(pyramid.base) }));
    }
    else {
        console.log('Pyramid not found');
    }
    // Поиск всех объектов
    const allRectangles = yield rectangleRepository.findAll();
    const allPyramids = yield pyramidRepository.findAll();
    console.log('All Rectangles:', allRectangles);
    console.log('All Pyramids:', allPyramids.map(p => (Object.assign(Object.assign({}, p), { base: formatPyramidBase(p.base) }))));
    // Пример сортировки объектов по ID
    const comparatorById = new ComparatorById();
    const sortedRectanglesById = allRectangles.sort((a, b) => comparatorById.compare(a, b));
    const sortedPyramidsById = allPyramids.sort((a, b) => comparatorById.compare(a, b));
    console.log('Sorted Rectangles by ID:', sortedRectanglesById);
    console.log('Sorted Pyramids by ID:', sortedPyramidsById.map(p => (Object.assign(Object.assign({}, p), { base: formatPyramidBase(p.base) }))));
    // Пример сортировки объектов по имени
    const comparatorByName = new ComparatorByName();
    const sortedRectanglesByName = allRectangles.sort((a, b) => comparatorByName.compare(a, b));
    const sortedPyramidsByName = allPyramids.sort((a, b) => comparatorByName.compare(a, b));
    console.log('Sorted Rectangles by Name:', sortedRectanglesByName);
    console.log('Sorted Pyramids by Name:', sortedPyramidsByName.map(p => (Object.assign(Object.assign({}, p), { base: formatPyramidBase(p.base) }))));
    // Пример сортировки объектов по координатам X первой точки
    const comparatorByFirstPointXForRectangle = new ComparatorByFirstPointXForRectangle();
    const sortedRectanglesByFirstPointX = allRectangles.sort((a, b) => comparatorByFirstPointXForRectangle.compare(a, b));
    const comparatorByFirstPointXForPyramid = new ComparatorByFirstPointXForPyramid();
    const sortedPyramidsByFirstPointX = allPyramids.sort((a, b) => comparatorByFirstPointXForPyramid.compare(a, b));
    console.log('Sorted Rectangles by First Point X:', sortedRectanglesByFirstPointX);
    console.log('Sorted Pyramids by First Point X:', sortedPyramidsByFirstPointX.map(p => (Object.assign(Object.assign({}, p), { base: formatPyramidBase(p.base) }))));
    // Пример сортировки объектов по координатам Y первой точки
    const comparatorByFirstPointYForRectangle = new ComparatorByFirstPointYForRectangle();
    const sortedRectanglesByFirstPointY = allRectangles.sort((a, b) => comparatorByFirstPointYForRectangle.compare(a, b));
    const comparatorByFirstPointYForPyramid = new ComparatorByFirstPointYForPyramid();
    const sortedPyramidsByFirstPointY = allPyramids.sort((a, b) => comparatorByFirstPointYForPyramid.compare(a, b));
    console.log('Sorted Rectangles by First Point Y:', sortedRectanglesByFirstPointY);
    console.log('Sorted Pyramids by First Point Y:', sortedPyramidsByFirstPointY.map(p => (Object.assign(Object.assign({}, p), { base: formatPyramidBase(p.base) }))));
    // Пример поиска объектов в первом квадранте
    const rectanglesInFirstQuadrant = allRectangles.filter(rect => rect.topLeft.x > 0 &&
        rect.topLeft.y > 0 &&
        rect.topRight.x > 0 &&
        rect.topRight.y > 0 &&
        rect.bottomRight.x > 0 &&
        rect.bottomRight.y > 0 &&
        rect.bottomLeft.x > 0 &&
        rect.bottomLeft.y > 0);
    const pyramidsInFirstQuadrant = allPyramids.filter(pyramid => pyramid.base.every(point => point.x > 0 && point.y > 0));
    console.log('Rectangles in the first quadrant:', rectanglesInFirstQuadrant);
    console.log('Pyramids in the first quadrant:', pyramidsInFirstQuadrant.map(p => (Object.assign(Object.assign({}, p), { base: formatPyramidBase(p.base) }))));
    // Пример поиска объектов по диапазону площади поверхности
    const minArea = 10;
    const maxArea = 50;
    const rectanglesInAreaRange = allRectangles.filter(rect => {
        const area = rect.getArea();
        return area >= minArea && area <= maxArea;
    });
    const pyramidsInAreaRange = allPyramids.filter(pyramid => {
        const surfaceArea = pyramid.getSurfaceArea();
        return surfaceArea >= minArea && surfaceArea <= maxArea;
    });
    console.log(`Rectangles with area between ${minArea} and ${maxArea}:`, rectanglesInAreaRange);
    console.log(`Pyramids with surface area between ${minArea} and ${maxArea}:`, pyramidsInAreaRange.map(p => (Object.assign(Object.assign({}, p), { base: formatPyramidBase(p.base) }))));
    // Пример поиска объектов по расстоянию от начала координат
    const minDistance = 0;
    const maxDistance = 10;
    const distanceFromOrigin = (point) => {
        return Math.sqrt(point.x * point.x + point.y * point.y + point.z * point.z);
    };
    const rectanglesInDistanceRange = allRectangles.filter(rect => {
        const distances = [
            distanceFromOrigin(rect.topLeft),
            distanceFromOrigin(rect.topRight),
            distanceFromOrigin(rect.bottomRight),
            distanceFromOrigin(rect.bottomLeft),
        ];
        return distances.every(distance => distance >= minDistance && distance <= maxDistance);
    });
    const pyramidsInDistanceRange = allPyramids.filter(pyramid => {
        const distances = pyramid.base.map(point => distanceFromOrigin(point));
        return distances.every(distance => distance >= minDistance && distance <= maxDistance);
    });
    console.log(`Rectangles within distance range ${minDistance} to ${maxDistance} from origin:`, rectanglesInDistanceRange);
    console.log(`Pyramids within distance range ${minDistance} to ${maxDistance} from origin:`, pyramidsInDistanceRange.map(p => (Object.assign(Object.assign({}, p), { base: formatPyramidBase(p.base) }))));
}))();
