import fs from 'fs';
import logger from '../logger/Logger';

export const readFile = (filePath: string) => {
    logger.info(`Reading file from ${filePath}`);
    const data = fs.readFileSync(filePath, 'utf-8');
    const lines = data.split('\n');
    logger.info(`Read ${lines.length} lines from ${filePath}`);
    return lines;
};