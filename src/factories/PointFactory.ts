import { Point } from '../entities/Point';

export class PointFactory {
    static create2DPoint(x: number, y: number): Point {
        return new Point(x, y);
    }

    static create3DPoint(x: number, y: number, z: number): Point {
        return new Point(x, y, z);
    }
}