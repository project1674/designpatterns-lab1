import { Rectangle } from '../entities/Rectangle';
import { Point } from '../entities/Point';

export class RectangleFactory {
  static createRectangle(id: string, name: string,topLeft: Point, topRight: Point, bottomRight: Point, bottomLeft: Point): Rectangle {
    if (this.isValidRectangle(topLeft, topRight, bottomRight, bottomLeft)) {
      return new Rectangle(id, name, topLeft, topRight, bottomRight, bottomLeft);
    }
    throw new Error('Four points do not form a rectangle.');
  }

  static isValidRectangle(topLeft: Point, topRight: Point, bottomRight: Point, bottomLeft: Point): boolean {
    let slope1 = (topRight.y - topLeft.y) / (topRight.x - topLeft.x);
    let slope2 = (bottomRight.y - topRight.y) / (bottomRight.x - topRight.x);
    let slope3 = (bottomLeft.y - bottomRight.y) / (bottomLeft.x - bottomRight.x);
    let slope4 = (topLeft.y - bottomLeft.y) / (topLeft.x - bottomLeft.x);

    return (Math.abs(slope1) === Math.abs(slope3)) && (Math.abs(slope2) === Math.abs(slope4));
  }
}


