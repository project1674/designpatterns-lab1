import { Pyramid } from '../entities/Pyramid';
import { Point } from '../entities/Point';

export class PyramidFactory {
  public create(id: string, name: string, base: Point[], height: number) {
    return new Pyramid(id, name, base, height);
  }
}