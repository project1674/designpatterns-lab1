import { Comparator } from "../interfaces/IComparator";
import { Rectangle } from "../../entities/Rectangle";

export class ComparatorByFirstPointXForRectangle implements Comparator<Rectangle> {
    compare(a: Rectangle, b: Rectangle): number {
        return a.topLeft.x - b.topLeft.x;
    }
}