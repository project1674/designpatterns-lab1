import { Comparator } from "../interfaces/IComparator";
import { Rectangle } from "../../entities/Rectangle";
import { Pyramid } from "../../entities/Pyramid";

export class ComparatorById<T extends { id: string }> implements Comparator<T> {
  compare(a: T, b: T): number {
    const idA = parseInt(a.id, 10);
    const idB = parseInt(b.id, 10);
    
    if (idA < idB) return -1;
    if (idA > idB) return 1;
    return 0;
  }
}