import { Comparator } from "../interfaces/IComparator";
import { Pyramid } from "../../entities/Pyramid";

export class ComparatorByFirstPointXForPyramid implements Comparator<Pyramid> {
  compare(a: Pyramid, b: Pyramid): number {
      return a.base[0].x - b.base[0].x;
  }
}