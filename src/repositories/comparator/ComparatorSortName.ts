import { Comparator } from "../interfaces/IComparator";
import { Rectangle } from "../../entities/Rectangle";
import { Pyramid } from "../../entities/Pyramid";

export class ComparatorByName<T extends { name: string }> implements Comparator<T> {
  compare(a: T, b: T): number {
      return a.name.localeCompare(b.name);
  }
}