import { Comparator } from "../interfaces/IComparator";
import { Rectangle } from "../../entities/Rectangle";

export class ComparatorByFirstPointYForRectangle implements Comparator<Rectangle> {
    compare(a: Rectangle, b: Rectangle): number {
        return a.topLeft.y - b.topLeft.y;
    }
}