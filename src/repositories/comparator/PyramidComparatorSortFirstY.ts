import { Comparator } from "../interfaces/IComparator";
import { Pyramid } from "../../entities/Pyramid";

export class ComparatorByFirstPointYForPyramid implements Comparator<Pyramid> {
  compare(a: Pyramid, b: Pyramid): number {
      return a.base[0].y - b.base[0].y;
  }
}
  