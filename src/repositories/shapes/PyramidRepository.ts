import { BaseRepository } from '../base/BaseRepository';
import { Pyramid } from '../../entities/Pyramid';

export class PyramidRepository extends BaseRepository<Pyramid> {
    private _store: Map<string, Pyramid> = new Map<string, Pyramid>();

    async create(item: Pyramid): Promise<boolean> {
        this._store.set(item.id, item);
        return this._store.has(item.id);
    }

    async update(id: string, item: Pyramid): Promise<boolean> {
        this._store.set(id, item);
        return this._store.has(id);
    }

    async delete(id: string): Promise<boolean> {
        return this._store.delete(id);
    }

    async find(item: Partial<Pyramid>): Promise<Pyramid[]> {
        return Array.from(this._store.values()).filter(pyramid => 
            (!item.id || pyramid.id === item.id) &&
            (!item.name || pyramid.name === item.name)
        );
    }

    async findOne(id: string): Promise<Pyramid | undefined> {
        return this._store.get(id);
    }

    async findAll(): Promise<Pyramid[]> {
        return Array.from(this._store.values());
    }
}
