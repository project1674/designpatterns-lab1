import { BaseRepository } from '../base/BaseRepository';
import { Rectangle } from '../../entities/Rectangle';

export class RectangleRepository extends BaseRepository<Rectangle> {
    private _store: Map<string, Rectangle> = new Map<string, Rectangle>();

    async create(item: Rectangle): Promise<boolean> {
        this._store.set(item.id, item);
        return this._store.has(item.id);
    }

    async update(id: string, item: Rectangle): Promise<boolean> {
        this._store.set(id, item);
        return this._store.has(id);
    }

    async delete(id: string): Promise<boolean> {
        return this._store.delete(id);
    }

    async find(item: Partial<Rectangle>): Promise<Rectangle[]> {
        return Array.from(this._store.values()).filter(rectangle => 
            (!item.id || rectangle.id === item.id) &&
            (!item.name || rectangle.name === item.name)
        );
    }

    async findOne(id: string): Promise<Rectangle | undefined> {
        return this._store.get(id);
    }

    async findAll(): Promise<Rectangle[]> {
        return Array.from(this._store.values());
    }
}
