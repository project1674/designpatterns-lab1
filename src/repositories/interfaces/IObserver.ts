export interface Observer {
    update(observable: Observable): void;
}

export interface Observable {
    registerObserver(observer: Observer): void;
    unregisterObserver(observer: Observer): void;
    notifyObservers(): void;
}