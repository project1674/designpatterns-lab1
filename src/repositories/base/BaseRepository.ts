import { IWrite } from "../interfaces/IWrite";
import { IRead } from "../interfaces/IRead";

export abstract class BaseRepository<T> implements IWrite<T>, IRead<T> {
    abstract find(item: Partial<T>): Promise<T[]>;
    abstract findOne(id: string): Promise<T | undefined>;
    abstract create(item: T): Promise<boolean>;
    abstract update(id: string, item: T): Promise<boolean>;
    abstract delete(id: string): Promise<boolean>;

    abstract findAll(): Promise<T[]>;
}
