import { Rectangle } from '../entities/Rectangle';
import { Pyramid } from '../entities/Pyramid';
import { Observer, Observable } from '../repositories/interfaces/IObserver';

interface RectangleData {
    getArea: () => number;
    isSquare: () => boolean;
    isRhombus: () => boolean;
}

interface PyramidData {
    getVolume: () => number;
    getBaseArea: () => number;
    getSlantArea: () => number;
    getSurfaceArea: () => number;
    getPerimeter: () => number;
    baseOnCoordinatePlane: () => string;
}

export class Warehouse implements Observer {
    private static instance: Warehouse;
    private rectangles: Map<string, RectangleData> = new Map();
    private pyramids: Map<string, PyramidData> = new Map();

    private constructor() {}

    public static getInstance(): Warehouse {
        if (!Warehouse.instance) {
            Warehouse.instance = new Warehouse();
        }
        return Warehouse.instance;
    }

    public registerRectangle(rectangle: Rectangle): void {
        rectangle.registerObserver(this);
        this.updateRectangle(rectangle);
    }

    public registerPyramid(pyramid: Pyramid): void {
        pyramid.registerObserver(this);
        this.updatePyramid(pyramid);
    }

    private updateRectangle(rectangle: Rectangle): void {
        this.rectangles.set(rectangle.id, {
            getArea: () => rectangle.getArea(),
            isSquare: () => rectangle.isSquare(),
            isRhombus: () => rectangle.isRhombus(),
        });
    }

    private updatePyramid(pyramid: Pyramid): void {
        this.pyramids.set(pyramid.id, {
            getVolume: () => pyramid.getVolume(),
            getBaseArea: () => pyramid.getBaseArea(),
            getSlantArea: () => pyramid.getSlantArea(),
            getSurfaceArea: () => pyramid.getSurfaceArea(),
            getPerimeter: () => pyramid.getPerimeter(),
            baseOnCoordinatePlane: () => pyramid.baseOnCoordinatePlane()
        });
    }

    public update(observable: Observable): void {
        if (observable instanceof Rectangle) {
            this.updateRectangle(observable);
        } else if (observable instanceof Pyramid) {
            this.updatePyramid(observable);
        }
    }

    // Дополнительные методы для доступа к данным
    public getRectangleData(id: string): RectangleData | undefined {
        return this.rectangles.get(id);
    }

    public getPyramidData(id: string): PyramidData | undefined {
        return this.pyramids.get(id);
    }

    public getAllRectangles(): Map<string, RectangleData> {
        return this.rectangles;
    }

    public getAllPyramids(): Map<string, PyramidData> {
        return this.pyramids;
    }
}

export default Warehouse;


