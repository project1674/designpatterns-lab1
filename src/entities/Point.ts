export class Point {
    constructor(public x: number, public y: number, public z: number = 0) { }

    distanceTo(otherPoint: Point) {
        const dx = otherPoint.x - this.x;
        const dy = otherPoint.y - this.y;
        const dz = otherPoint.z - this.z;
        return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }
}