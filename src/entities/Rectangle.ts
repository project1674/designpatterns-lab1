import { Point } from './Point';
import { Observer, Observable } from '../repositories/interfaces/IObserver';

export class Rectangle implements Observable {
    observers: Observer[] = [];
    constructor(public id: string, public name: string, public topLeft: Point, public topRight: Point, public bottomRight: Point, public bottomLeft: Point ) {
        this.id = id;
    }
    
    getArea(): number {
        const width = this.topRight.distanceTo(this.topLeft);
        const height = this.topRight.distanceTo(this.bottomRight);

        return width * height;
    }

    getPerimeter(): number {
        const width = this.topRight.distanceTo(this.topLeft);
        const height = this.topRight.distanceTo(this.bottomRight);
        
        return 2 * (width + height);
    }

    isSquare(): boolean {
        const sides = [
            this.topLeft.distanceTo(this.topRight),
            this.topRight.distanceTo(this.bottomRight),
            this.bottomRight.distanceTo(this.bottomLeft),
            this.bottomLeft.distanceTo(this.topLeft),
        ];

        return sides.every(side => side === sides[0]);
    }

    isRhombus(): boolean {
        return this.isSquare();
    }

    registerObserver(observer: Observer): void {
        this.observers.push(observer);
    }
    
    unregisterObserver(observer: Observer): void {
        const removeIndex = this.observers.findIndex(obs => obs === observer);
            if (removeIndex !== -1) {
                this.observers.splice(removeIndex, 1);
            }
    }
    
      notifyObservers(): void {
        this.observers.forEach(observer => observer.update(this));
    }
}
