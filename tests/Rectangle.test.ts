import { RectangleFactory } from '../src/factories/RectangleFactory';
import { PointFactory } from '../src/factories/PointFactory';
import { describe, test, expect, beforeEach } from "@jest/globals"
import { Rectangle } from '../src/entities/Rectangle';

describe('Rectangle Test', () => {
    let rectangle: Rectangle;

    beforeEach(() => {
        const topLeft = PointFactory.create2DPoint(0, 0);
        const topRight = PointFactory.create2DPoint(5, 0);
        const bottomRight = PointFactory.create2DPoint(5, 5);
        const bottomLeft = PointFactory.create2DPoint(0, 5);
        rectangle = RectangleFactory.createRectangle('1', 'Test Rectangle', topLeft, topRight, bottomRight, bottomLeft);
    });

    test('Test area of Rectangle', () => {
        expect(rectangle.getArea()).toEqual(25);
    });

    test('Test perimeter of Rectangle', () => {
        expect(rectangle.getPerimeter()).toEqual(20);
    });

    test('Test isSquare', () => {
        expect(rectangle.isSquare()).toBe(true);
    });

    test('Check if Rectangle is a Rhombus', () => {
        expect(rectangle.isRhombus()).toBe(true);
    });

    describe("Rectangle Form Test", () => {
        const topLeft = PointFactory.create2DPoint(0, 0);
        const topRight = PointFactory.create2DPoint(2, 0);
        const bottomRight = PointFactory.create2DPoint(2, 2);
        const bottomLeft = PointFactory.create2DPoint(0, 2);
        const invalidPoint = PointFactory.create2DPoint(1, 2);

        test('Points form a rectangle', () => {
            expect(RectangleFactory.isValidRectangle(topLeft, topRight, bottomRight, bottomLeft)).toBe(true);
        });

        test('Points do not form a rectangle', () => {
            expect(RectangleFactory.isValidRectangle(topLeft, topRight, invalidPoint, bottomLeft)).toBe(false);
        });
    });
});
