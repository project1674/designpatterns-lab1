import { PyramidFactory } from '../src/factories/PyramidFactory';
import { PointFactory } from '../src/factories/PointFactory';
import { describe, expect, beforeEach } from "@jest/globals"
import { Pyramid } from '../src/entities/Pyramid';

describe('Pyramid Test', () => {
    let pyramid: Pyramid;
    const pyramidFactory = new PyramidFactory();

    describe('Pyramid with rectangle base', () => {
        beforeEach(() => {
            const basePoints = [
                PointFactory.create2DPoint(0, 0), 
                PointFactory.create2DPoint(0, 1), 
                PointFactory.create2DPoint(1, 1), 
                PointFactory.create2DPoint(1, 0)
            ];
            pyramid = pyramidFactory.create("1","Test Pyramid", basePoints, 1);
        });

        test('Base area of Pyramid', () => {
            expect(pyramid.getBaseArea()).toEqual(1);
        });

        test('Surface area of Pyramid', () => {
            expect(pyramid.getSurfaceArea()).toBeCloseTo(3.236, 3);
        });

        test('Volume of Pyramid', () => {
            expect(pyramid.getVolume()).toBeCloseTo(0.333, 3);
        });
    });

    describe('Pyramid with triangle base', () => {
        beforeEach(() => {
            const basePoints = [
                PointFactory.create2DPoint(0, 0), 
                PointFactory.create2DPoint(1, 0), 
                PointFactory.create2DPoint(0, 1)
            ];
            pyramid = pyramidFactory.create("2", "Test Pyramid", basePoints, 1);
        });

        test('Base area of Pyramid', () => {
            expect(pyramid.getBaseArea()).toBeCloseTo(0.5, 2);
        });

        test('Surface area of Pyramid', () => {
            expect(pyramid.getSurfaceArea()).toBeCloseTo(2.484, 2);
        });

        test('Volume of Pyramid', () => {
            expect(pyramid.getVolume()).toBeCloseTo(0.167, 3);
        });
    });

    describe('Check if base of pyramid is on coordinate plane', () => {
        test('YZ plane', () => {
            pyramid = pyramidFactory.create(
                "3",
                "Test Pyramid", 
                [
                    PointFactory.create3DPoint(0, 1, 2), 
                    PointFactory.create3DPoint(0, 3, 4), 
                    PointFactory.create3DPoint(0, 5, 6)
                ], 
                1
            ); 
            expect(pyramid.baseOnCoordinatePlane()).toEqual('YZ');
        });

        test('XZ plane', () => {
            pyramid = pyramidFactory.create(
                "4",
                "Test Pyramid", 
                [
                    PointFactory.create3DPoint(1, 0, 2), 
                    PointFactory.create3DPoint(3, 0, 4), 
                    PointFactory.create3DPoint(5, 0, 6), 
                    PointFactory.create3DPoint(7, 0, 8)
                ], 
                2
            );
            expect(pyramid.baseOnCoordinatePlane()).toEqual('XZ');
        });

        test('XY plane', () => {
            pyramid = pyramidFactory.create(
                "5",
                "Test Pyramid", 
                [
                    PointFactory.create3DPoint(1, 2, 0), 
                    PointFactory.create3DPoint(3, 4, 0), 
                    PointFactory.create3DPoint(5, 6, 0)
                ], 
                3
            );
            expect(pyramid.baseOnCoordinatePlane()).toEqual('XY');
        });
        
        test('No plane', () => {
            pyramid = pyramidFactory.create(
                "6",
                "Test Pyramid", 
                [
                    PointFactory.create3DPoint(1, 2, 3), 
                    PointFactory.create3DPoint(4, 5, 6), 
                    PointFactory.create3DPoint(7, 8, 9)
                ], 
                4
            );
            expect(pyramid.baseOnCoordinatePlane()).toEqual('no plane');
        });
    });

});
